var hotelsDate = require('../data/hotel-data.json');

module.exports.hotelsGetAll = (req, res) => {
    console.log('GET the hotels');
    res
        .status(200)
        .json(hotelsDate);
};