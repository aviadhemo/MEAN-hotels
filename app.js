const express = require('express');
const app = express();
const path = require('path');

const routes = require('./api/routes');

app.set('port', 3000);

app.use((req, res, next) => {
    console.log(req.method, req.url);
    next();
});

app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', routes);


var server = app.listen(app.get('port'), () => {
    var port = server.address().port;
    console.log(`Magic happens in port ${port}`);
});

